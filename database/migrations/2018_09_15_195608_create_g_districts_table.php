<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_districts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',20);
            $table->integer('state_id');
            $table->foreign('state_id','g_district_stateid_fk')->references('id')->on('g_states');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_districts');
    }
}
